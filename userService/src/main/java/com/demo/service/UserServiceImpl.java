package com.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.demo.entity.User;

@Service
public class UserServiceImpl implements UserService {

	List<User> list = List.of(new User(22L, "Sameer", "5664"), new User(23L, "Ajit", "1234"),
			new User(24L, "Raju", "1122"), new User(25L, "Sagar", "1243"), new User(26L, "Suresh", "1344"));

	@Override
	public User getUser(Long id) {
		// TODO Auto-generated method stub
		return list.stream().filter(user -> user.getUserId().equals(id)).findAny().orElse(null);
	}

}
