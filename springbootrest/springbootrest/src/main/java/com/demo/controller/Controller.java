package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.entity.Course;
import com.demo.service.CourseServiceImpl;

@RestController
public class Controller {

	@Autowired
	private CourseServiceImpl cs;

	@GetMapping("/course")
	public List<Course> getCourse() {
		return this.cs.getCourse();

	}

	@PostMapping("/course")
	public Course addCourse(@RequestBody Course course) {
		return this.cs.addCourse(course);

	}

	@PutMapping("/course/{courseId}")
	public Course updateCourse(@RequestBody Course course) {
		return this.cs.updateCourse(course);

	}

	@DeleteMapping("/course/{courseId}")
	public ResponseEntity<HttpStatus> deleteCourse(@PathVariable String courseId) {
		try {
			this.cs.deleteCourse(Long.parseLong(courseId));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
