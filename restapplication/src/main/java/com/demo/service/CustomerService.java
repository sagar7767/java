package com.demo.service;

import java.util.List;

import com.demo.model.Customer;

public interface CustomerService {

	public List<Customer> getCustomers();
	
	public Customer addCustomer(Customer customer);
	
	public Customer updateCustomer(Customer customer);
	
	public void deleteCustomer(int customerId);

}
