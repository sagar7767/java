package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.exception.BusinessException;
import com.demo.model.Customer;
import com.demo.repo.CustomerRepo;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepo custRepo;

	@Override
	public List<Customer> getCustomers() {
		return custRepo.findAll();
	}

	@Override
	public Customer addCustomer(Customer customer) {
		if(customer.getCustName().isEmpty()||customer.getCustName().length()==0) {
			throw new BusinessException("601","Please enter proper name");
		}
		try {
			custRepo.save(customer);
			return customer;
		}catch (IllegalArgumentException e) {
			throw new BusinessException("602", "Given customer is null");
			// TODO: handle exception
		}catch(Exception e) {
			throw new BusinessException("603", "Some thing is wrong here");
		}
		
	}

	@Override
	public Customer updateCustomer(Customer customer) {
		custRepo.save(customer);
		return customer;
	}

	@Override
	public void deleteCustomer(int customerId) {
		custRepo.deleteById(customerId);
	}

}
