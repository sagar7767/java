package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.exception.BusinessException;
import com.demo.exception.ControllerException;
import com.demo.model.Customer;
import com.demo.service.CustomerServiceImpl;

@RestController
public class Controller {

	@Autowired
	private CustomerServiceImpl custServiceImpl;

	@GetMapping("/customers")
	public List<Customer> getCustomers() {
		return custServiceImpl.getCustomers();
	}

//	@PostMapping("/customers")
//	public Customer addCustomer(@RequestBody Customer customer) {
//		return custServiceImpl.addCustomer(customer);
//	}
	@PostMapping("/customers")
	public ResponseEntity<?> addCustomer(@RequestBody Customer customer){
		try {
			Customer custSave = custServiceImpl.addCustomer(customer);
			return new ResponseEntity<Customer>(custSave,HttpStatus.CREATED);
		}catch(BusinessException e) {
			ControllerException ce = new ControllerException(e.getErrorCode(),e.getErrorMessage());
			return new ResponseEntity<ControllerException>(ce,HttpStatus.BAD_REQUEST);
		}catch(Exception e) {
			ControllerException ce = new ControllerException("611","Something Went Wrong");
			return new ResponseEntity<ControllerException>(ce,HttpStatus.BAD_REQUEST);
		
		}
	}

	@PutMapping("/customers/{custId}")
	public Customer updateCustomer(@RequestBody Customer customer) {
		return custServiceImpl.updateCustomer(customer);
	}

	@DeleteMapping("/customers/{custId}")
	public ResponseEntity<HttpStatus> deleteCustomer(@PathVariable int custId) {
		try {
			this.custServiceImpl.deleteCustomer(custId);
			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
