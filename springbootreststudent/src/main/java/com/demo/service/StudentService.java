package com.demo.service;

import java.util.List;

import com.demo.entity.Student;

public interface StudentService {

	public List<Student> getStudents();
	
	public Student addStudent(Student student);
	
	public Student updateStudent(Student student);
	
	public void deleteStudent(long studentId);

	//public Student getStudentById(long studentId);
}
