package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dao.StudentDao;
import com.demo.entity.Student;

@Service
public class StudentServiceImpl implements StudentService{

	@Autowired
	private StudentDao studentDao;
	
	@Override
	public List<Student> getStudents() {
		// TODO Auto-generated method stub
		return this.studentDao.findAll();
	}

	@Override
	public Student addStudent(Student student) {
		// TODO Auto-generated method stub
		studentDao.save(student);
		return student;
	}

	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		studentDao.save(student);
		return student;
	}

	@Override
	public void deleteStudent(long studentId) {
		// TODO Auto-generated method stub
		Student entity = studentDao.getOne(studentId);
		studentDao.delete(entity);
	}

//	@Override
//	public Student getStudentById(long studentId) {
//		// TODO Auto-generated method stub
//		return this.studentDao.findById(studentId);
//	}

	

}
