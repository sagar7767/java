package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.entity.Student;
import com.demo.service.StudentServiceImpl;

@RestController
public class Controller {

	@Autowired
	private StudentServiceImpl studentServiceImpl;
	
	@GetMapping("/student")
	public List<Student> getStudent(){
		return this.studentServiceImpl.getStudents();
		
	}
	
//	@GetMapping("/student/{studentId}")
//	public List<Student> getStudentOne(@PathVariable("studentID") long studentId){
//		return this.studentServiceImpl.getStudentById(studentId);
//		
//	}
	
	@PostMapping("/student")
	public Student addStudent(@RequestBody Student student) {
		return this.studentServiceImpl.addStudent(student);
	}
	
	@PutMapping("/student/{studentId}")
	public Student updateStudent(@RequestBody Student student) {
		return this.studentServiceImpl.updateStudent(student);
	}
	
	@DeleteMapping("/student/{studentId}")
	public ResponseEntity<HttpStatus> deleteStudent(@PathVariable String studentId) {
		try {
			this.studentServiceImpl.deleteStudent(Long.parseLong(studentId));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
