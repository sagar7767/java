package com.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.demo.entity.Contact;

@Service
public class ContactServiceImpl implements ContactService {

	List<Contact> list = List.of(new Contact(1L, "Sameer@gmail.com", 22L), new Contact(2L, "Ajit@gmail.com", 23L),
			new Contact(3L, "Raju@gmail.com", 24L), new Contact(4L, "Sagar@gmail.com", 25L),
			new Contact(4L, "Suresh@gmail.com", 26L));

	@Override
	public List<Contact> getContactsOfUser(Long userId) {
		// TODO Auto-generated method stub
		return list.stream().filter(contact -> contact.getUserId().equals(userId)).collect(Collectors.toList());
	}

}
