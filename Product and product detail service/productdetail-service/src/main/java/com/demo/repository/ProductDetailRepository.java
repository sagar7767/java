package com.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.entity.ProductDetails;

@Repository
public interface ProductDetailRepository extends JpaRepository<ProductDetails, Long>{
	ProductDetails findByProductDetailId(Long productDetailId);
}
