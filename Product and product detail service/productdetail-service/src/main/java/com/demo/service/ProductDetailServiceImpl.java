package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.entity.ProductDetails;
import com.demo.repository.ProductDetailRepository;

@Service
public class ProductDetailServiceImpl implements ProductDetailService {

	@Autowired
	private ProductDetailRepository productDetailRepository;

	public ProductDetails getProductDetailsById(Long productDetailId) {
		return productDetailRepository.findByProductDetailId(productDetailId);
	}

	@Override
	public List<ProductDetails> getProductDetails() {
		return productDetailRepository.findAll();
	}

	@Override
	public ProductDetails addProductDetails(ProductDetails productDetails) {
		return productDetailRepository.save(productDetails);
	}

	@Override
	public ProductDetails updateProductDetails(ProductDetails productDetails) {
		return productDetailRepository.save(productDetails);
	}

	@Override
	public void deleteProductDetails(Long productDetailId) {
		productDetailRepository.deleteById(productDetailId);

	}

}
