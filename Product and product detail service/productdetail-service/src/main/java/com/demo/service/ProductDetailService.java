package com.demo.service;

import java.util.List;

import com.demo.entity.ProductDetails;

public interface ProductDetailService {

	public List<ProductDetails> getProductDetails();

	public ProductDetails addProductDetails(ProductDetails productDetails);

	public ProductDetails updateProductDetails(ProductDetails productDetails);

	public void deleteProductDetails(Long productDetailId);

}
