package com.demo.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetails {
	private Long productDetailId;
	private String manufacturerName;
	private String contactNumber;

//	public ProductDetails() {
//		super();
//	}
//
//	public ProductDetails(Long productDetailId, String manufacturerName, String contactNumber) {
//		super();
//		this.productDetailId = productDetailId;
//		this.manufacturerName = manufacturerName;
//		this.contactNumber = contactNumber;
//	}
//
//	public Long getProductDetailId() {
//		return productDetailId;
//	}
//
//	public void setProductDetailId(Long productDetailId) {
//		this.productDetailId = productDetailId;
//	}
//
//	public String getManufacturerName() {
//		return manufacturerName;
//	}
//
//	public void setManufacturerName(String manufacturerName) {
//		this.manufacturerName = manufacturerName;
//	}
//
//	public String getContactNumber() {
//		return contactNumber;
//	}
//
//	public void setContactNumber(String contactNumber) {
//		this.contactNumber = contactNumber;
//	}
//
//	@Override
//	public String toString() {
//		return "ProductDetails [productDetailId=" + productDetailId + ", manufacturerName=" + manufacturerName
//				+ ", contactNumber=" + contactNumber + "]";
//	}

}
