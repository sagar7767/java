package com.demo.service;

import java.util.List;

import com.demo.entity.Product;

public interface ProductService {

	public List<Product> getAllProducts();
	
	public Product addProduct(Product product);
	
	public Product updateProduct(Product product);
	
	public void deleteProduct(Long productId);
}
